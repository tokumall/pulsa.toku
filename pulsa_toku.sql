/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost
 Source Database       : pulsa_toku

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : utf-8

 Date: 05/09/2017 22:32:02 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `produk`
-- ----------------------------
DROP TABLE IF EXISTS `produk`;
CREATE TABLE `produk` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) DEFAULT NULL,
  `jenis` varchar(20) DEFAULT NULL,
  `provider` varchar(20) DEFAULT NULL,
  `nama` text,
  `harga_beli` int(7) DEFAULT NULL,
  `harga_personal` int(7) DEFAULT NULL,
  `harga_agen` int(7) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `produk`
-- ----------------------------
BEGIN;
INSERT INTO `produk` VALUES ('1', 'IP5', 'pulsa', 'indosat', 'INDOSAT 5.000', '5525', '6500', '6000', 'aktif', '2017-05-06 09:34:31', '2017-05-06 09:34:31'), ('3', 'PL20', 'token', 'pln', 'PLN 20.000', '19830', '21500', '21000', 'aktif', '2017-05-02 11:38:07', '2017-05-02 11:38:07'), ('4', 'IP10', 'pulsa', 'indosat', 'INDOSAT 10.000', '10525', '11500', '11000', 'aktif', '2017-05-06 09:25:36', '2017-05-06 09:25:36'), ('5', 'SP5', 'pulsa', 'telkomsel', 'TELKOMSEL 5.000', '5775', '6500', '6000', 'aktif', '2017-05-06 09:32:20', '2017-05-06 09:32:20');
COMMIT;

-- ----------------------------
--  Table structure for `sale`
-- ----------------------------
DROP TABLE IF EXISTS `sale`;
CREATE TABLE `sale` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `kode_produk` varchar(10) DEFAULT NULL,
  `pembeli` varchar(25) DEFAULT NULL,
  `id_user` int(5) DEFAULT NULL,
  `no_handphone` varchar(13) DEFAULT NULL,
  `harga_jual` int(25) DEFAULT NULL,
  `harga_beli` int(25) DEFAULT NULL,
  `kode_unik` int(3) DEFAULT NULL,
  `pembayaran` varchar(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `sale`
-- ----------------------------
BEGIN;
INSERT INTO `sale` VALUES ('5', 'KRSBV', '2017-05-08', 'IP5', 'tester', '1', null, '6500', '5525', '43', 'bca', '1', '2017-05-08 17:37:12'), ('6', 'IZFNY', '2017-05-08', 'IP5', 'tester', '1', null, '6500', '5525', '8', 'bca', '2', '2017-05-08 17:37:27'), ('7', 'HCNTP', '2017-05-08', 'IP5', 'tester', '1', null, '6500', '5525', '21', 'bca', '3', '2017-05-08 17:42:46'), ('8', 'KPUQI', '2017-05-08', 'IP5', 'tester', '1', null, '6500', '5525', '22', 'bca', '4', '2017-05-08 17:43:28'), ('9', 'BTIZR', '2017-05-08', 'IP5', 'tester', '1', '085890567859', '6500', '5525', '37', 'bca', '5', '2017-05-08 17:54:23'), ('10', 'GYHUT', '2017-05-08', 'PL20', 'tester', '1', '085890567859', '21500', '19830', '14', 'bca', '1', '2017-05-08 18:29:19');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
