<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('mata_uang')) {
	function mata_uang($value='')
	{
        $jadi = "Rp " .number_format($value,0,',','.');
        return $jadi;		
	}
}

if (!function_exists('option_month')) {
    function option_month($value='')
    {
        $bulans = array(
            '1' => 'Januari', 
            '2' => 'Februari', 
            '3' => 'Maret',
            '4' => 'April',
            '5' => 'Mei',
            '6' => 'Juni',
            '7' => 'Juli',
            '8' => 'Agustus',
            '9' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember');

        $option = '';
        foreach ($bulans as $nomor => $bulan) {
            $select = '';
            if ($nomor == $value) {
                $select = "selected";
            }
            $option .= '<option value="'.$nomor.'"'.$select.'>'.$bulan.'</option>';
        }
        return $option;
    }
}
if (!function_exists('option_angka')) {
    function option_angka($value='')
    {
        $bulans = array(
            '1' => '1', 
            '2' => '2', 
            '3' => '3',
            '4' => '4',
            '5' => '5',
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10',
            '11' => '11',
            '12' => '12');

        $option = '';
        foreach ($bulans as $nomor => $bulan) {
            $select = '';
            if ($nomor == $value) {
                $select = "selected";
            }
            $option .= '<option value="'.$nomor.'"'.$select.'>'.$bulan.'</option>';
        }
        return $option;
    }
}
if (!function_exists('option_tahun')) {
    function option_tahun($value='')
    {
        for ($i=2014; $i <= date('Y'); $i++) { 
            $select = '';
            if ($i == date('Y')) {
                $select = "selected";
            }
            $option .= '<option value="'.$i.'"'.$select.'>'.$i.'</option>';
        }
        return $option;
    }
}
if (!function_exists('option_jenis')) {
    function option_jenis($selected)
    {
        $option = '';
        $data = array(
            'pulsa' => 'PULSA', 
            'data' => 'DATA', 
            'token' => 'TOKEN', 
            'voucher' => 'VOUCHER', 
            );
        foreach ($data as $key => $value) {
            $select = ($key == $selected) ? "selected" : "" ;        
            $option .= '<option value="'.$key.'"'.$select.'>'.$value.'</option>';
        }
        return $option;
    }
}
if (!function_exists('option_provider')) {
    function option_provider($data, $selected)
    {
        $option = '';
        foreach ($data as $key => $value) {
            $select = ($value == $selected) ? "selected" : "" ;        
            $option .= '<option value="'.$value.'"'.$select.'>'.strtoupper($value).'</option>';
        }
        return $option;
    }
}
if (!function_exists('option_status')) {
    function option_status($selected)
    {
        $option = '';
        $data = array(
            1 => 'Pending', 
            2 => 'Proses',
            3 => 'Berhasil',
            4 => 'Batal',
            5 => 'Refund',
            );
        foreach ($data as $key => $value) {
            $select = ($key == $selected) ? "selected" : "" ;        
            $option .= '<option value="'.$key.'"'.$select.'>'.$value.'</option>';
        }
        return $option;
    }
}
if (!function_exists('status_transaksi')) {
	function status_transaksi($status)
	{
        switch ($status) {
            case 1:
                $return = '<span class="badge badge-warning badge-icon"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Menunggu Pembayaran</span></span>';
                break;
            case 2:
                $return = '<span class="badge badge-info badge-icon"><i class="fa fa-credit-card" aria-hidden="true"></i><span>Sedang di Proses</span></span>';
                break;
            case 3:
                $return = '<span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Transaksi Berhasil</span></span>';
                break;
            case 4:
                $return = '<span class="badge badge-danger badge-icon"><i class="fa fa-times" aria-hidden="true"></i><span>Dibatalkan</span></span>';
                break;
            case 5:
                $return = '<span class="badge badge-primary badge-icon"><i class="fa fa-truck" aria-hidden="true"></i><span>Refund</span></span>';
                break;
            default:
                $return = '';
                break;
        }
        return $return;;
	}
}
