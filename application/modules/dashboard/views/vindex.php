<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <a class="card card-banner card-green-light">
            <div class="card-body">
                <i class="icon fa fa-user fa-4x"></i>
                <div class="content">
                <div class="title">Total Anggota</div>
                <div class="value"><span class="sign"></span>420</div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <a class="card card-banner card-yellow-light">
            <div class="card-body">
                <i class="icon fa fa-money fa-4x"></i>
                <div class="content">
                <div class="title">Telat Pembayaran</div>
                <div class="value"><span class="sign"></span>20 </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
        <a class="card card-banner card-blue-light">
            <div class="card-body">
                <i class="icon fa fa-usd fa-4x"></i>
                <div class="content">
                <div class="title">Konfirmasi Pembayaran</div>
                <div class="value"><span class="sign"></span>5</div>
                </div>
            </div>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Pembayaran Telat
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID Anggota</th>
                      <th>Nama</th>
                      <th>Belum Bayar</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>201701</td>
                      <td>System Architect</td>
                      <td>2 Bulan</td>
                      <td>
                        <a href="<?= base_url('pembayaran/anggota')?>" class="btn btn-success btn-sm">Bayar</a>
                      </td>
                  </tr>
              </tbody>
          </table>
        </div>
      </div>
    </div>
    
</div>