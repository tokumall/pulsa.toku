<div class="row">
  <div class="col-xs-12">
    <div class="card">
      <div class="card-header">
        
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-md-4">
            <select class="select2">
              <option value="2017">2017</option>
              <option value="2016">2016</option>
              <option value="2015">2015</option>
            </select>            
          </div>
          <div class="col-md-4">
            <select class="select2">
              <?= option_month() ?>
            </select>        
          </div>
          <div class="col-md-4">
            <input type="button" name="btn_pembayaran" class="btn btn-primary" value="Cari">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Pembayaran
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID Anggota</th>
                      <th>Nama</th>
                      <th>Status</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>201701</td>
                      <td>System Architect</td>
                      <td>
                        <span class="badge badge-danger badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span>Belum</span></span>
                      </td>
                      <td>
                        <a href="" class="btn btn-primary btn-sm">Bayar</a>
                      </td>
                  </tr>
              </tbody>
          </table>
        </div>
      </div>
    </div>    
</div>