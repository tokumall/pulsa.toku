<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body app-heading">
        <img class="profile-img" src="<?= base_url('images/profile.png')?>">
        <div class="app-title">
          <div class="title">
            <span class="highlight">Rendy Panji Ramadhan</span>
          </div>
          <div class="description">00000212391918</div>
          <div class="description">00000212391918</div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Pembayaran Iuran
        </div>
        <div class="card-body">
          <form class="form form-horizontal">
            <div class="form-group">
              <label class="col-md-3 control-label">Untuk Pembayaran</label>
              <div class="col-md-9">
                <div class="input-group">
                  <select class="select2">
                    <?= option_angka() ?>
                  </select>
                  <span class="input-group-addon">Bulan</span>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>    
</div>