<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfirmasi_pembayaran extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['page'] = array('list_pembayaran','Konfirmasi Pembayaran','anggota');
		$this->template->view('Konfirmasi_pembayaran/vindex', $data);
	}
	public function check_invoice($id)
	{
		$this->load->model('konfirmasi_pembayaran_model');
		$sale = $this->konfirmasi_pembayaran_model->sale($id);
		$invoice = $this->konfirmasi_pembayaran_model->invoice($id);
		if (count($sale) != 1) {
			echo 'No invoice tidak ditemukan';
		}else if (count($invoice) != 0){
			echo 'Konfirmasi sudah dilakukan sebelumnya';
		}
		
	}
	public function submit()
	{
		$invoice = $this->input->post('invoice');
		$bank_asal = $this->input->post('bank_asal');
		$pemilik = $this->input->post('pemilik');
		$transfer = $this->input->post('transfer');
		$jumlah = $this->input->post('jumlah');
		$tanggal = $this->input->post('tanggal');

		$data = array(
			'invoice' => $invoice,
			'bank_asal' => $bank_asal,
			'pemilik' => $pemilik,
			'transfer'=> $transfer,
			'jumlah'=> $jumlah,
			'tanggal'=> $tanggal,
			'created_at' => date('Y-m-d H:i:s'), 
			);
		$insert = $this->db->insert('invoice', $data);

		if ($insert) {
			$content = array('pembayaran' => 'Rp. ', 'bank' => 'Mandiri', 'no_rek' => '12345678', 'atas_nama' => 'Rendy Panji');
			$return = array('status' => 'berhasil', 'data' => 'konfirmasi_pembayaran', 'konten' => $content);
			echo json_encode($return);
		}
	}
}
