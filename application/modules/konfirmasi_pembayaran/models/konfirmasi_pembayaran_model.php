<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class konfirmasi_pembayaran_model extends CI_Model
{
	function show($id)
	{
		$data = $this->db->get_where('sale', array('status' => $id ));
		return $data->result_array();
	}
	function sale($value='')
	{
		$data = $this->db->get_where('sale', array('kode' => $value, ));
		return $data->result_array();
	}
	function invoice($value='')
	{
		$data = $this->db->get_where('invoice', array('invoice' => $value, ));
		return $data->result_array();
	}
}