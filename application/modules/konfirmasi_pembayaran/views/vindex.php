<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Pulsa
        </div>
        <div class="card-body ">
        <div class="loader-container text-center">
            <div class="icon">
                <div class="sk-wave">
                    <div class="sk-rect sk-rect1"></div>
                    <div class="sk-rect sk-rect2"></div>
                    <div class="sk-rect sk-rect3"></div>
                    <div class="sk-rect sk-rect4"></div>
                    <div class="sk-rect sk-rect5"></div>
                  </div>
            </div>
            <div class="title">Loading</div>
        </div>
          <form class="form form-horizontal" method="POST" id="konfirmasi_pembayaran" action="<?= base_url('konfirmasi_pembayaran/submit')?>">
            <div class="section">
              <div class="section-title">Pilih </div>
              <div class="section-body">
                <div class="form-group">
                  <label class="col-md-3 control-label">Invoice Order</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="invoice" onchange="checkInvoice(this.value)">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Bank Asal</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="bank_asal">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Pemilik Rekening</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="pemilik">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Transfer Ke</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2" id="pembayaran" name="transfer">
                        <option value="bca">BCA</option>
                        <option value="mandiri">MANDIRI</option>
                        <option value="bni">BNI</option>
                        <option value="bri">BRI</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Jumlah</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="jumlah">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Tanggal Transfer</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="tanggal" value="<?= date('Y-m-d')?>">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-footer">
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <span type="submit" class="btn btn-primary" onclick="form_submit('konfirmasi_pembayaran','Segera Lakukan Transfer')">Konfirmasi Pembayaran Saya</span>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>	
</div>