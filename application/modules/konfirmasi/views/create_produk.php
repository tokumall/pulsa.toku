<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Produk
        </div>
        <div class="card-body ">
        <div class="loader-container text-center">
            <div class="icon">
                <div class="sk-wave">
                    <div class="sk-rect sk-rect1"></div>
                    <div class="sk-rect sk-rect2"></div>
                    <div class="sk-rect sk-rect3"></div>
                    <div class="sk-rect sk-rect4"></div>
                    <div class="sk-rect sk-rect5"></div>
                  </div>
            </div>
            <div class="title">Loading</div>
        </div>
          <form class="form form-horizontal">
            <div class="section">
              <div class="section-title">Tambah Produk</div>
              <div class="section-body">
                <div class="form-group">
                  <label class="col-md-3 control-label">Kode Produk</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Jenis Produk</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2">
                        <option value="1">PULSA</option>
                        <option value="2">PAKET DATA</option>
                        <option value="2">TOKEN</option>
                        <option value="2">VOUCHER GAME</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Provider</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2">
                        <option value="1">INDOSAT</option>
                        <option value="2">TELKOMSEL</option>
                        <option value="2">XL</option>
                        <option value="2">AXIS</option>
                        <option value="2">BOLT</option>
                        <option value="2">SMARTFREN</option>
                        <option value="2">THREE</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Nama Produk</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Harga Beli</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Harga Personal</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Harga Agen</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Status</label>
                  <div class="col-md-9">
                    <div class="radio radio-inline">
                        <input type="radio" name="radio4" id="radio10" value="option10">
                        <label for="radio10">
                          Aktif
                        </label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" name="radio4" id="radio11" value="option11" checked>
                        <label for="radio11">
                          Non Aktif
                        </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-footer">
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-default">Cancel</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>	
</div>