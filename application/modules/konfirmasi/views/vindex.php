<div class="row proses">
        <div class="loader-container text-center">
            <div class="icon">
                <div class="sk-wave">
                    <div class="sk-rect sk-rect1"></div>
                    <div class="sk-rect sk-rect2"></div>
                    <div class="sk-rect sk-rect3"></div>
                    <div class="sk-rect sk-rect4"></div>
                    <div class="sk-rect sk-rect5"></div>
                  </div>
            </div>
            <div class="title">Loading</div>
        </div>
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
        Daftar Konfirmasi Pulsa
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%" id="konfirmasi">
              <thead>
                  <tr>
                      <th>Kode Invoice</th>
                      <th>Tanggal</th>
                      <th>Bank Asal</th>
                      <th>Pemilik Rekening</th>
                      <th>Jumlah (Rp)</th>
                      <th>Rekening Tujuan</th>
                      <th width="500px">Status</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
<?php foreach ($transaksi as $key => $value): ?>
                  <tr>
                      <td><?= $value['invoice']?></td>
                      <td><?= $value['tanggal'] ?></td>
                      <td><?= $value['bank_asal'] ?></td>
                      <td><?= $value['pemilik'] ?></td>
                      <td><?= $value['jumlah'] ?></td>
                      <td><?= $value['transfer'] ?></td>
                      <td>
                      <select class="select2" id="statusincoive" name="statusinvoice" onchange="updateStatus('<?=$value['invoice']?>',this)">
                        <?= option_status(1) ?>
                      </select>
                      </td>
                      <td>
                        <a href="" class="btn btn-primary btn-xs">Invoice</a>
                        <span href="" class="btn btn-success btn-xs" onclick="click_kirim('<?=$value['invoice']?>')">Kirim</span>
                        <a href="" class="btn btn-danger btn-xs">Hapus</a>
                      </td>
                  </tr>
  
<?php endforeach ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>    
</div>