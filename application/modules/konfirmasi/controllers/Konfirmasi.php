<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfirmasi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('konfirmasi_model');
		$data['transaksi'] = $this->konfirmasi_model->show();

		$data['page'] = array('Konfirmasi','Konfirmasi Pembayaran','konfirmasi');
		$this->template->view('konfirmasi/vindex',$data);

	}
	public function updateStatus($id='', $data)
	{
		if($id != ''){
			$data = array('status' => $data, );
			$this->db->where('kode',$id);
			$status = $this->db->update('sale', $data);
			$return = 'failed';
			if ($status) {
				$return = "done";
			}
			echo $return;
		}
	}
	public function data()
	{
		$id_user = 1;
		$this->load->model('konfirmasi_model');
		$sale = $this->konfirmasi_model->sale();
		$transaksi = $this->konfirmasi_model->show($id_user);
		foreach ($transaksi as $key => $value) {
			foreach ($sale as $key1 => $value1) {
				if ($value['kode'] == $value1['kode']) {
					$status = "<input type=\"checkbox\" name=\"\" ".$check." onChange=\"updatePublish('".$value['kode']."',this)\">";
					$aksi = "<a class=\"btn btn-success btn-xs\" href=\"".base_url('transaksi/invoice/'.$value['kode'])."\">Invoice</a>";
					
					$data['data'][] = array(
						'kode' => $value['kode'],
						'tanggal' => $value['tanggal'],
						'nomor' => $value['no_handphone'],
						'total' => $value['harga_jual']+$value['kode_unik'],
						'pembayaran' => strtoupper($value['pembayaran']),
						'status' => status_transaksi($value['status']) ,
						'aksi' => $aksi,
						);
				}
			}
			// var_dump($value['id']);
		}


		echo json_encode($data);
	}
}
