<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Konfirmasi_model extends CI_Model
{
	function show()
	{
		$query = "SELECT invoice.invoice, 
	invoice.bank_asal, 
	invoice.pemilik, 
	invoice.transfer, 
	invoice.jumlah, 
	invoice.tanggal, 
	invoice.created_at, 
	invoice.id
FROM invoice INNER JOIN sale ON invoice.invoice = sale.kode
WHERE sale.`status` = 1";
		$data = $this->db->query($query);
		return $data->result_array();
	}
	function get($value='')
	{
		$data = $this->db->get_where('sale', array('kode' => $value, ));
		return $data->result_array();
	}
}