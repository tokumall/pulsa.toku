<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beli extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['page'] = array('beli','Beli Pulsa','beli');
		$this->template->view('beli/vindex',$data);
	}
	public function jenis($value='',$inner='')
	{
		$produk = array(
			'pulsa' => array(
				'indosat',
				'telkomsel',
				'xl',
				'axis',
				'bolt',
				'smartfren',
				'three' 
				),
			'data' => array(
				'indosat',
				'telkomsel',
				'xl',
				'axis',
				'bolt',
				'smartfren',
				'three' 
				),
			'token' => array('pln'),	
			'voucher' => array(
				'zynga',
				'megaxus',
				'gamescool'), 
			);
		if ($inner == 'in') {
			return $produk[$value];
		} else {
			echo json_encode($produk[$value]);
		}	
	}
	public function voucher($jenis='',$provider='', $inner='')
	{
		$value = array('jenis' => $jenis, 'provider' => $provider, 'status'=> 'aktif' );
		$this->load->model('beli_model');
		$data = $this->beli_model->get_voucher($value);
		if ($inner == 'in'){
			print_r($data) ;
		}else{
			echo json_encode($data);
		}
	}
	public function harga($id)
	{	
		$value = array('id' => $id, 'status'=>'aktif');
		$this->load->model('beli_model');
		$data = $this->beli_model->get_harga($value);
		echo json_encode($data);
	}
	public function pulsa()
	{
		$voucher = $this->input->post('voucher');
		$handphone = $this->input->post('handphone');
		$pembayaran = $this->input->post('pembayaran');

		$this->load->model('beli_model');
		$produk = $this->beli_model->get_produk($voucher);
		$kode_unik = rand(1,99);
		$this->load->helper('string');
		$kd_invoice = strtoupper(random_string('alpha', 5));

		$invoice = array(
			'kode' => $kd_invoice, 
			'tanggal' => date('Y-m-d'), 
			'kode_produk' => $produk->kode, 
			'id_user' => 1, 
			'pembeli' => 'tester',
			'no_handphone' => $handphone, 
			'pembayaran' => $pembayaran, 
			'kode_unik' => $kode_unik,
			'harga_beli' => $produk->harga_beli,
			'harga_jual' => $produk->harga_personal,
			'status' => 1, 
			'created_at' => date('Y-m-d H:i:s'), 
			);
		$insert = $this->db->insert('sale', $invoice);

		if ($insert) {
			$content = array('pembayaran' => 'Rp. '.($produk->harga_personal+$kode_unik), 'bank' => 'Mandiri', 'no_rek' => '12345678', 'atas_nama' => 'Rendy Panji');
			$return = array('status' => 'berhasil', 'data' => 'beli_pulsa', 'konten' => $content);
			echo json_encode($return);
		}

	}
}
