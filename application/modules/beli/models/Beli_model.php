<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Beli_model extends CI_Model
{
	function get_voucher($data)
	{
		$this->db->select('id, nama');
		$return = $this->db->get_where('produk', $data);
		return $return->result_array();
	}
	function get_harga($data)
	{
		$this->db->select('harga_personal');
		$return = $this->db->get_where('produk', $data);
		return $return->result_array();
	}
	function get_produk($id)
	{
		$return = $this->db->get_where('produk', array('id' => $id, ));
		return $return->row();
	}
}