<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Pulsa
        </div>
        <div class="card-body ">
        <div class="loader-container text-center">
            <div class="icon">
                <div class="sk-wave">
                    <div class="sk-rect sk-rect1"></div>
                    <div class="sk-rect sk-rect2"></div>
                    <div class="sk-rect sk-rect3"></div>
                    <div class="sk-rect sk-rect4"></div>
                    <div class="sk-rect sk-rect5"></div>
                  </div>
            </div>
            <div class="title">Loading</div>
        </div>
          <form class="form form-horizontal" method="POST" id="beli_pulsa" action="<?= base_url('beli/pulsa')?>">
            <div class="section">
              <div class="section-title">Pilih </div>
              <div class="section-body">
                <div class="form-group">
                  <label class="col-md-3 control-label">Jenis Produk</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2" id="jenis" name="jenis">
                        <option value="pilih">PILIH</option>
                        <option value="pulsa">PULSA</option>
                        <option value="data">PAKET DATA</option>
                        <option value="token">TOKEN</option>
                        <option value="voucher">VOUCHER GAME</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Provider</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2" id="provider" name="provider">
                        <option value="pilih">PILIH</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Voucher</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2" id="voucher" name="voucher" onchange="harga_voucher(this.value)">
                        <option value="pilih">PILIH</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Nomor Handphone</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="handphone">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Metode Pembayaran</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2" id="pembayaran" name="pembayaran">
                        <option value="bca">BCA</option>
                        <option value="mandiri">MANDIRI</option>
                        <option value="bni">BNI</option>
                        <option value="bri">BRI</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 col-md-offset-3 harga">Rp. 0</label>
                </div>
              </div>
            </div>
            <div class="form-footer">
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <span type="submit" class="btn btn-primary" onclick="form_submit('beli_pulsa','Segera Lakukan Transfer')">Beli Sekarang</span>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>	
</div>