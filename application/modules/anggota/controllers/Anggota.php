<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class anggota extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['page'] = array('list_pembayaran','Anggota','anggota');
		$this->template->view('anggota/list_anggota', $data);
	}
	public function create($value='')
	{
		$data['page'] = array('create_anggota', 'Anggota Baru', 'anggota' );
		$this->template->view('anggota/create_anggota', $data);
	}
	public function created_at($value='')
	{
		if ($this->input->post('nama')) {

			$join = $this->input->post('join_tahun').'-'.$this->input->post('join_bulan').'-01';


			$data = array(
				'nama' => $this->input->post('nama'),
				'tempat_lahir' => $this->input->post('tempat'),
				'tanggal_lahir' => $this->input->post('tanggal'),
				'gender' => $this->input->post('gender'),
				'agama' => $this->input->post('agama'),
				'alamat' => $this->input->post('alamat'),
				'email' => $this->input->post('email'),
				'handphone' => $this->input->post('handphone'),
				'telepon' => $this->input->post('telepon'),
				'kk_l' => $this->input->post('kk_laki'),
				'kk_p' => $this->input->post('kk_perem'),
				'tempat_tinggal' => $this->input->post('tempat_tinggal'),
				'join' => $join,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
				);

			// $insert = $this->anggota->insert($data);
			$insert='';
			if($insert == 'done'){
				echo 'done';
			}else{
				echo 'failed_again';
			}
		}else{
			echo 'failed';
		}
	}
}
