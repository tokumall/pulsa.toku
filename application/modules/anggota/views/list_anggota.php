<div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
          Anggota
          <div class="card-action">
            <a href="" class="btn btn-primary">Tambah</a>
          </div>
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>ID Anggota</th>
                      <th>Nama</th>
                      <th>Nomor KK</th>
                      <th>Aksi</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>201701</td>
                      <td>System Architect</td>
                      <td>1281716125241</td>
                      <td>
                        <a href="" class="btn btn-info btn-sm">Ubah</a>
                        <a href="<?= base_url('pembayaran/anggota')?>" class="btn btn-success btn-sm">Bayar</a>
                      </td>
                  </tr>
              </tbody>
          </table>
        </div>
      </div>
    </div>    
</div>