<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Formulir Pendaftaran
        </div>
        <div class="card-body ">
        <div class="loader-container text-center">
            <div class="icon">
                <div class="sk-wave">
                    <div class="sk-rect sk-rect1"></div>
                    <div class="sk-rect sk-rect2"></div>
                    <div class="sk-rect sk-rect3"></div>
                    <div class="sk-rect sk-rect4"></div>
                    <div class="sk-rect sk-rect5"></div>
                  </div>
            </div>
            <div class="title">Loading</div>
        </div>
          <form class="form form-horizontal">
            <div class="section">
              <div class="section-title">Data Pribadi</div>
              <div class="section-body">
                <div class="form-group">
                  <label class="col-md-3 control-label">Nama Lengkap</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Tempat Lahir</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Tanggal Lahir</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Jenis Kelamin</label>
                  <div class="col-md-9">
                    <div class="radio radio-inline">
                        <input type="radio" name="radio4" id="radio10" value="option10">
                        <label for="radio10">
                          Laki-laki
                        </label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" name="radio4" id="radio11" value="option11" checked>
                        <label for="radio11">
                          Perempuan
                        </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Agama</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2">
                        <option value="1">Islam</option>
                        <option value="2">Kristen</option>
                        <option value="3">Budha</option>
                        <option value="4">Hindu</option>
                        <option value="4">Konghucu</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-3">
                    <label class="control-label">Alamat</label>
                    <p class="control-label-help">Masukkan Alamat Lengkap</p>
                  </div>
                  <div class="col-md-9">
                    <textarea class="form-control"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Email</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Nomor Hanphone</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Nomor Telepon</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Anggota Keluarga</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <input type="text" class="form-control" name="">
                      <span class="input-group-addon">Laki-Laki</span>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group">
                      <input type="text" class="form-control" name="">
                      <span class="input-group-addon">Perempuan</span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Tempat Tinggal</label>
                  <div class="col-md-9">
                    <div class="radio radio-inline">
                        <input type="radio" name="radio4" id="radio10" value="option10">
                        <label for="radio10">
                          Tetap
                        </label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" name="radio4" id="radio11" value="option11" checked>
                        <label for="radio11">
                          Pengontrak
                        </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Bulan Bergabung</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2">
                        <?= option_month(date('m')) ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2">
                        <?= option_tahun() ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Upload Foto</label>
                  <div class="col-md-9">
                    <input type="file" class="form-control" placeholder="">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-footer">
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-default">Cancel</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>	
</div>