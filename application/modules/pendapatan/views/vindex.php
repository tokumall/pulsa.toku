<div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
        Daftar Penjualan
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>Kode Invoice</th>
                      <th>Tanggal</th>
                      <th>Kode Produk</th>
                      <th>Pembeli</th>
                      <th>Total (Rp)</th>
                      <th>Pembayaran</th>
                      <th>Status</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>10219291</td>
                      <td>29/04/2017</td>
                      <td>IX20</td>
                      <td>RENDY PANJI</td>
                      <td>21000</td>
                      <td>BCA</td>
                      <td>
                        <span class="badge badge-success badge-icon"><i class="fa fa-check" aria-hidden="true"></i><span> BERHASIL</span></span> 
                      </td>
                      <td>
                        <a href="" class="btn btn-primary btn-xs">Invoice</a>
                        <a href="" class="btn btn-danger btn-xs">Hapus</a>
                      </td>
                  </tr>
              </tbody>
          </table>
        </div>
      </div>
    </div>    
</div>