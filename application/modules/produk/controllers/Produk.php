<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['page'] = array('produk','Produk','produk');
		$this->template->view('produk/vindex',$data);
	}
	public function create($value='')
	{
		$data['page'] = array('produk','Tambah Produk','produk' );
		$this->template->view('produk/create_produk', $data);
	}
	public function edit($value='')
	{
		// $this->input->post()
		$this->load->model('produk_model');
		$data['data'] = $this->produk_model->get($value)[0];
		$data['data']['aktif'] = '';
		$data['data']['non_aktif'] = '';
		$data['data']['list_provider'] = $this->jenis($data['data']['jenis'],'in');
		// var_dump($data['data']['list_provider']);
		$retVal = ($data['data']['status'] == "aktif") ? $data['data']['aktif'] = 'checked' : $data['data']['non_aktif'] = 'checked' ;
		$data['page'] = array('produk','Edit Produk','produk' );
		$this->template->view('produk/edit_produk', $data);
	}
	public function updateStatus($id='', $data)
	{
		if($id != ''){
			$status = "non_aktif";
			if ($data == "true") {
				$status = "aktif";
			}
			$data = array('status' => $status, );
			$this->db->where('kode',$id);
			$status = $this->db->update('produk', $data);
			$return = 'failed';
			if ($status) {
				$return = "done";
			}
			echo $return;
		}
	}
	public function remove($id='')
	{
		if($id != ''){
			$status = $this->db->delete('produk', array('kode' => $id, ));
			$return = 'failed';
			if ($status) {
				$return = "done";
			}
			echo $return;
		}
	}
	public function data()
	{
		$this->load->model('produk_model');
		$produk = $this->produk_model->show();
		foreach ($produk as $key => $value) {
			$check = '';
			if ($value['status'] == 'aktif') {
				$check = 'checked';
			}
			$status = "<input type=\"checkbox\" name=\"\" ".$check." onChange=\"updatePublish('".$value['kode']."',this)\">";
			$aksi = "<a class=\"btn btn-warning btn-xs\" href=\"".base_url('produk/edit/'.$value['kode'])."\">Edit</a>
			<span class=\"btn btn-danger btn-xs\" onCLick=\"deleteProduct('".$value['kode']."')\">Delete</span>";
			
			$data['data'][] = array(
				'kode' => $value['kode'],
				'jenis' => strtoupper($value['jenis']),
				'provider' => strtoupper($value['provider']),
				'nama' => $value['nama'],
				'harga_beli' => $value['harga_beli'],
				'harga_personal' => $value['harga_personal'],
				'status' => $status ,
				'aksi' => $aksi,
				);
			// var_dump($value['id']);
		}


		echo json_encode($data);
	}
	public function add($value='')
	{
		if ($this->input->post('kode') == NULL) {
			echo 'gagal';
		}else{
		$data = array(
			'kode' => $this->input->post('kode'), 
			'jenis' => $this->input->post('jenis'),
			'provider' => $this->input->post('provider'),
			'nama' => $this->input->post('nama'),
			'harga_beli' => $this->input->post('harga_beli'),
			'harga_personal' => $this->input->post('harga_personal'),
			'harga_agen' => $this->input->post('harga_agen'),
			'status' => $this->input->post('status'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
			);
		// $insert = TRUE;
		$insert = $this->db->insert('produk',$data);
		if ($insert) {
			echo 'berhasil';
		}else{
			echo "gagal";
		}			
		}
	}
	public function update()
	{
		if ($this->input->post('kode') == NULL) {
			echo 'gagal';
		}else{
		$data = array(
			'kode' => $this->input->post('kode'), 
			'jenis' => $this->input->post('jenis'),
			'provider' => $this->input->post('provider'),
			'nama' => $this->input->post('nama'),
			'harga_beli' => $this->input->post('harga_beli'),
			'harga_personal' => $this->input->post('harga_personal'),
			'harga_agen' => $this->input->post('harga_agen'),
			'status' => $this->input->post('status'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
			);
		// $insert = TRUE;
		$this->db->set($data);
		$this->db->where('kode', $data['kode']);
		$insert = $this->db->update('produk',$data);
		if ($insert) {
			echo 'berhasil';
		}else{
			echo "gagal";
		}			
		}
	}
	public function jenis($value='',$inner='')
	{
		$produk = array(
			'pulsa' => array(
				'indosat',
				'telkomsel',
				'xl',
				'axis',
				'bolt',
				'smartfren',
				'three' 
				),
			'data' => array(
				'indosat',
				'telkomsel',
				'xl',
				'axis',
				'bolt',
				'smartfren',
				'three' 
				),
			'token' => array('pln'),	
			'voucher' => array(
				'zynga',
				'megaxus',
				'gamescool'), 
			);
		if ($inner == 'in') {
			return $produk[$value];
		} else {
			echo json_encode($produk[$value]);
		}
		
	}
}
