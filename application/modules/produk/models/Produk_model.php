<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Produk_model extends CI_Model
{
	function show()
	{
		$data = $this->db->get('produk');
		return $data->result_array();
	}
	function get($value='')
	{
		$data = $this->db->get_where('produk', array('kode' => $value, ));
		return $data->result_array();
	}
}