<div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
        Daftar Produk
        </div>
        <div class="card-body no-padding">
          <table class="table table-striped primary" cellspacing="0" width="100%" id="produk_pulsa">
              <thead>
                  <tr>
                      <th>Kode Produk</th>
                      <th>Jenis</th>
                      <th>Provider</th>
                      <th>Nama Produk</th>
                      <th>Harga Beli</th>
                      <th>Harga Personal</th>
                      <th>Publish</th>
                      <th></th>
                  </tr>
              </thead>
          </table>
        </div>
      </div>
    </div>    
</div>