<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          Produk
        </div>
        <div class="card-body ">
        <div class="loader-container text-center">
            <div class="icon">
                <div class="sk-wave">
                    <div class="sk-rect sk-rect1"></div>
                    <div class="sk-rect sk-rect2"></div>
                    <div class="sk-rect sk-rect3"></div>
                    <div class="sk-rect sk-rect4"></div>
                    <div class="sk-rect sk-rect5"></div>
                  </div>
            </div>
            <div class="title">Loading</div>
        </div>
          <form class="form form-horizontal" method="POST" id="create_produk" action="<?= base_url('produk/add')?>">
            <div class="section">
              <div class="section-title">Tambah Produk</div>
              <div class="section-body">
                <div class="form-group">
                  <label class="col-md-3 control-label">Kode Produk</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="kode">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Jenis Produk</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2" id="jenis" name="jenis">
                        <option value="pilih">PILIH</option>
                        <option value="pulsa">PULSA</option>
                        <option value="data">PAKET DATA</option>
                        <option value="token">TOKEN</option>
                        <option value="voucher">VOUCHER GAME</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Provider</label>
                  <div class="col-md-4">
                    <div class="input-group">
                      <select class="select2" id="provider" name="provider">
                        <option value="pilih">PILIH</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Nama Produk</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="nama">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Harga Beli</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="harga_beli">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Harga Personal</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="harga_personal">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Harga Agen</label>
                  <div class="col-md-9">
                    <input type="text" class="form-control" placeholder="" name="harga_agen">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label">Status</label>
                  <div class="col-md-9">
                    <div class="radio radio-inline">
                        <input type="radio" name="status" id="radio10" value="aktif">
                        <label for="radio10">
                          Aktif
                        </label>
                    </div>
                    <div class="radio radio-inline">
                        <input type="radio" name="status" id="radio11" value="non_aktif" checked>
                        <label for="radio11">
                          Non Aktif
                        </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-footer">
              <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                  <span type="submit" class="btn btn-primary" onclick="submit_produk('create_produk','Produk berhasil ditambahkan')">Tambah</span>
                  <span type="button" class="btn btn-default">Cancel</span>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>	
</div>