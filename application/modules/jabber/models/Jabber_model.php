<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jabber_model extends CI_Model
{
	function get($value='')
	{
		$data = $this->db->get_where('sale', array('kode' => $value, ));
		return $data->result_array();
	}
	function setting()
	{
		$options = new Fabiang\Xmpp\Options('tcp://jabbim.cz:5222');
		$options->setUsername('toku@jabbim.cz')
		    ->setPassword('tokumall123$');
		return $options;
	}
	function client()
	{
		$client = new Fabiang\Xmpp\Client($this->setting());
		return $client;		
	}
	function connect()
	{
		// optional connect manually
		$this->client()->connect();
	}
	function message($var)
	{
		$this->connect();
		// send a message to another user
		$message = new Fabiang\Xmpp\Protocol\Message;
		$message->setMessage($var)
		    // ->setTo('cyg7889@jabbim.cz');
		    ->setTo('tokumall@jabber.cz');
		$this->client()->send($message);
		$this->disconnect();
	}
	function disconnect()
	{
		$this->client()->disconnect();
	}
	function pulsa($kode,$no)
	{
		$this->message($kode.'.'.$no.'.1234');
	}
}