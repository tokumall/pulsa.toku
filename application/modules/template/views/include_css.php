  <link rel="stylesheet" type="text/css" href="<?= base_url('css/vendor.css')?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('css/flat-admin.css')?>">

  <!-- Theme -->
  <link rel="stylesheet" type="text/css" href="<?= base_url('css/theme/blue-sky.css')?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('css/theme/blue.css')?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('css/theme/red.css')?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('css/theme/yellow.css')?>">
