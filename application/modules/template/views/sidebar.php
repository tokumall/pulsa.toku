<?php 
  switch ($page[2]) {
    case 'dashboard':
      $dashboard = 'active';
      break;
    case 'produk':
      $produk = 'active';
      break;
    case 'penjualan':
      $penjualan = 'active';
      break;
    case 'pembayaran':
      $pembayaran = 'active';
      break;
    case 'konfirmasi':
      $konfirmasi = 'active';
      break;
    case 'beli':
      $beli = 'active';
      break;
    case 'transaksi':
      $transaksi_side = 'active';
      break;
    case 'konfirmasi_pembayaran':
      $konfirmasi_pembayaran = 'active';
      break;
    default:
      # code...
      break;
  }
?>
    <ul class="sidebar-nav">
      <li class="<?= @$dashboard ?>">
        <a href="<?= base_url() ?>">
          <div class="icon">
            <i class="fa fa-tasks" aria-hidden="true"></i>
          </div>
          <div class="title">Dashboard</div>
        </a>
      </li>
      <li class="dropdown <?= @$produk ?>">
        <a href="<?= base_url('produk')?>" class="dropdown-toggle" data-toggle="dropdown">
          <div class="icon">
            <i class="fa fa-cube" aria-hidden="true"></i>
          </div>
          <div class="title">Produk</div>
        </a>
        <div class="dropdown-menu">
          <ul>
            <li><a href="<?= base_url('produk')?>">List Produk</a></li>
            <li><a href="<?= base_url('produk/create')?>">Tambah Produk</a></li>
          </ul>
        </div>
      </li>
      <li class="dropdown <?= @$penjualan ?>">
        <a href="<?= base_url('penjualan')?>" class="dropdown-toggle" data-toggle="dropdown">
          <div class="icon">
            <i class="fa fa-cube" aria-hidden="true"></i>
          </div>
          <div class="title">Produk</div>
        </a>
        <div class="dropdown-menu">
          <ul>
            <li><a href="<?= base_url('penjualan')?>">Berhasil</a></li>
            <li><a href="<?= base_url('penjualan/waiting')?>">Menunggu</a></li>
            <li><a href="<?= base_url('penjualan/process')?>">Memproses</a></li>
            <li><a href="<?= base_url('penjualan/cancel')?>">Dibatalkan</a></li>
            <li><a href="<?= base_url('penjualan/refund')?>">Refund</a></li>
          </ul>
        </div>
      </li>
      <li class="<?= @$konfirmasi ?>">
        <a href="<?= base_url('konfirmasi')?>">
          <div class="icon">
            <i class="fa fa-user" aria-hidden="true"></i>
          </div>
          <div class="title">Konfirmasi</div>
        </a>
      </li>
      <li class="<?= @$beli ?>">
        <a href="<?= base_url('beli')?>">
          <div class="icon">
            <i class="fa fa-user" aria-hidden="true"></i>
          </div>
          <div class="title">Beli</div>
        </a>
      </li>
      <li class="<?= @$transaksi_side ?>">
        <a href="<?= base_url('transaksi')?>">
          <div class="icon">
            <i class="fa fa-user" aria-hidden="true"></i>
          </div>
          <div class="title">History Transaksi</div>
        </a>
      </li>
      <li class="<?= @$konfirmasi_pembayaran ?>">
        <a href="<?= base_url('konfirmasi_pembayaran')?>">
          <div class="icon">
            <i class="fa fa-user" aria-hidden="true"></i>
          </div>
          <div class="title">Konfirmasi Pembayaran</div>
        </a>
      </li>
    </ul>
