  <script type="text/javascript" src="<?= base_url('js/vendor.js') ?>"></script>
  <script type="text/javascript">
  	base_url = '<?= base_url()?>';
  </script>
  <script type="text/javascript" src="<?= base_url('js/app.js') ?>"></script>
  <script type="text/javascript">
  $("#jenis").on('change', function () {
  	jenis = this.value;
    $.ajax({
      url : base_url+'produk/jenis/'+this.value,
      success: function(data) {
        var obj = JSON.parse(data);
        $('#provider').empty();
        $.each(obj, function(key, x){
	    	$("#provider").select2({'data':[{id:x,text:x.toUpperCase()}]});
        })
        list_provider($("#provider").eq(0).val());
      },
      error: function(e) {
        console.log(e)
      }
    });
  });
  $("#provider").on('change', function () {
  	list_provider(this.value);
  });
  	function list_provider(provider) {
  	$.ajax({
  		url : base_url+'beli/voucher/'+jenis+"/"+provider,
  		success: function (data) {
  			// console.log(data);
  			var obj = JSON.parse(data);
  			$('#voucher').empty();
  			$.each(obj, function(key, x) {
  				$("#voucher").select2({'data':[{id:x.id,text:x.nama}]});
  			})
  			harga_voucher($("#voucher").eq(0).val());
  		},
  		error: function(e) {
  			console.log(e)
  		}
  	});
  	}
  	function harga_voucher(voucher) {
  	$.ajax({
  		url : base_url+'beli/harga/'+voucher,
  		success: function (data) {
  			// console.log(data);
  			var obj = JSON.parse(data);
  			$.each(obj, function(key, x) {
  				console.log(x);
  				$(".harga").text("Rp. "+x.harga_personal);
  			})
  		},
  		error: function(e) {
  			console.log(e)
  		}
  	});
  	}


  var datatable = $('#produk_pulsa').DataTable({
    "dom": '<"top"fl<"clear">>rt<"bottom"ip<"clear">>',
    "scrollCollapse": true,
    "scrollY":        "300px",
    "oLanguage": {
      "sSearch": "",
      "sLengthMenu": "_MENU_"
    },
    "initComplete": function initComplete(settings, json) {
      $('div.dataTables_filter input').attr('placeholder', 'Search...');
    },
    "serverSide": false,
        "ajax": {
          "url": base_url+'/produk/data',
        },
        "columns": [
        {data: "kode"},
        {data: "jenis"},
        {data: "provider"},
        {data: "nama"},
        {data: "harga_beli"},
        {data: "harga_personal"},
        {data: "status"},
        {data: "aksi"},
        ],
    buttons: [
                'excel', 'pdf', 'print'
            ]
  });
  var datatable = $('#history').DataTable({
	  "dom": '<"top"fl<"clear">>rt<"bottom"ip<"clear">>',
	  "scrollCollapse": true,
	  "scrollY":        "300px",
	  "oLanguage": {
	    "sSearch": "",
	    "sLengthMenu": "_MENU_"
	  },
	  "initComplete": function initComplete(settings, json) {
	    $('div.dataTables_filter input').attr('placeholder', 'Search...');
	  },
		"serverSide": false,
        "ajax": {
        	"url": base_url+'transaksi/data',
        },
        "columns": [
        {data: "kode"},
        {data: "tanggal"},
        {data: "nomor"},
        {data: "total"},
        {data: "pembayaran"},
        {data: "status"},
        {data: "aksi"},
        ],
		buttons: [
		            'excel', 'pdf', 'print'
		        ]
	});

  function form_submit(form_id, notif){
	var form = $('#'+form_id);
    var formdata = false;
    if (window.FormData){
        formdata = new FormData(form[0]);
    }
    $.ajax({
    	url: form.attr('action'),
    	type: 'POST',
    	dataType: 'html',
    	data: formdata ? formdata : form.serialize(),
        cache       : false,
        contentType : false,
        processData : false,
        beforeSend: function () {
        	
        },
        success: function (data) {
        	memproses(data);
        }
    });

  }
  function memproses(data) {
  	var obj = JSON.parse(data);
	if(obj.status == 'berhasil'){
		switch(obj.data){
			case 'beli_pulsa':
				var konten = obj.konten;
				$('.nominal').text(konten.pembayaran);
				$('.bank').text(konten.bank);
				$('.no_rek').text(konten.no_rek);
				$('.atas_nama').text(konten.atas_nama);
				$('#myModal').modal('show');
				break;
      case 'konfirmasi_pembayaran':
        noty("Terima kasih sudah melakukan konfirmasi pembayaran. Data anda segera kami proses.",'success','transaksi');
        break;
		}
	} else {
	}
  }  		
  </script>
  <script type="text/javascript">
  function submit_produk(form_id, notif){
  var form = $('#'+form_id);
    var formdata = false;
    if (window.FormData){
        formdata = new FormData(form[0]);
    }
    $.ajax({
      url: form.attr('action'),
      type: 'POST',
      dataType: 'html',
      data: formdata ? formdata : form.serialize(),
        cache       : false,
        contentType : false,
        processData : false,
        beforeSend: function () {
          
        },
        success: function (data) {
          noty(notif,'success','produk');
        }
    });

  }
    // admin - update publish 
  function updatePublish(id, data) {
    $.get(base_url+"produk/updateStatus/"+id+"/"+data.checked, function(data, status){
      });
    }
  function updateStatus(id, data) {
      console.log('update '+id+' = '+data.value);
    $.get(base_url+"konfirmasi/updateStatus/"+id+"/"+data.value, function(data, status){
        noty('update status berhasil','success','konfirmasi');
      });
    }
  function checkInvoice(id) {
    $.get(base_url+"konfirmasi_pembayaran/check_invoice/"+id, function(data, status){
      console.log('update '+id+' = '+data);
      if(data !== ''){
        noty(data,'warning','konfirmasi_pembayaran');
      }
      });
    }
    function deleteProduct(id) {
  var n = new Noty({
    layout: 'center',
    theme: 'sunset',
    text: 'Do you want to remove?',
    buttons: [
      Noty.button('YES', 'btn btn-success', function () {
      $.get(base_url+"produk/remove/"+id, function(data, status){
        console.log('delete '+id+' = '+data);
        });
        $('#produk_pulsa').DataTable().ajax.reload();
          n.close();
      }, {id: 'button1', 'data-status': 'ok'}),

      Noty.button('NO', 'btn btn-error', function () {
          console.log('button 2 clicked');
          n.close();
      })
    ]
  }).show();
    }
    function click_kirim(var1) {
        $(".proses").addClass("__loading");
        $.get(base_url+"jabber/kirim/"+var1, function(data, status){
          noty('sudah dikirim','success','konfirmasi');
        });
    }
  </script>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Permintaan Berhasil</h4>
      </div>
      <div class="modal-body">
        <p>Silahkan melakukan Transfer Sebesar : <label class="nominal"></label></p>
        <p>Ke Bank : <label class="bank"></label></p>
        <p>Nomor Rekening : <label class="no_rek"></label></p>
        <p>Atas Nama : <label class="atas_nama"></label></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-default" onclick="window.location.href = base_url+'transaksi';">Close</button>
        <button type="button" class="btn btn-sm btn-success">Cetak Invoice</button>
      </div>
    </div>
  </div>
</div>
