<div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
        History Transaksi
        </div>
        <div class="card-body no-padding">
          <table class="table table-striped primary" cellspacing="0" width="100%" id="history">
              <thead>
                  <tr>
                      <th>Kode Invoice</th>
                      <th>Tanggal</th>
                      <th>No Tujuan</th>
                      <th>Total</th>
                      <th>Pembayaran</th>
                      <th>Status</th>
                      <th></th>
                  </tr>
              </thead>
          </table>
        </div>
      </div>
    </div>    
</div>