<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['page'] = array('Transaksi','History Transaksi','transaksi');
		$this->template->view('transaksi/vindex',$data);
	}
	public function data()
	{
		$id_user = 1;
		$this->load->model('transaksi_model');
		$transaksi = $this->transaksi_model->show($id_user);
		foreach ($transaksi as $key => $value) {
			$check = '';
			if ($value['status'] == 'aktif') {
				$check = 'checked';
			}
			$status = "<input type=\"checkbox\" name=\"\" ".$check." onChange=\"updatePublish('".$value['kode']."',this)\">";
			$aksi = "<a class=\"btn btn-success btn-xs\" href=\"".base_url('transaksi/invoice/'.$value['kode'])."\">Invoice</a>";
			
			$data['data'][] = array(
				'kode' => $value['kode'],
				'tanggal' => $value['tanggal'],
				'nomor' => $value['no_handphone'],
				'total' => $value['harga_jual']+$value['kode_unik'],
				'pembayaran' => strtoupper($value['pembayaran']),
				'status' => status_transaksi($value['status']) ,
				'aksi' => $aksi,
				);
			// var_dump($value['id']);
		}


		echo json_encode($data);
	}
}
