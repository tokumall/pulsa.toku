<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transaksi_model extends CI_Model
{
	function show($id)
	{
		$data = $this->db->get_where('sale', array('id_user' => $id, ));
		return $data->result_array();
	}
	function get($value='')
	{
		$data = $this->db->get_where('sale', array('kode' => $value, ));
		return $data->result_array();
	}
}