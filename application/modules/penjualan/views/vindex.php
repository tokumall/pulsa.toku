<div class="row">
    <div class="col-xs-12">
      <div class="card">
        <div class="card-header">
        Daftar Penjualan
        </div>
        <div class="card-body no-padding">
          <table class="datatable table table-striped primary" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th>Kode Invoice</th>
                      <th>Tanggal</th>
                      <th>Kode Produk</th>
                      <th>Pembeli</th>
                      <th>Total (Rp)</th>
                      <th>Pembayaran</th>
                      <th>Status</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
              <?php foreach ($penjualan as $value): ?>
                  <tr>
                      <td><?= $value['kode']?></td>
                      <td><?= $value['tanggal']?></td>
                      <td><?= $value['kode_produk']?></td>
                      <td><?= $value['pembeli']?></td>
                      <td><?= 'Rp. '.($value['harga_jual']+$value['kode_unik'])?></td>
                      <td><?= strtoupper($value['pembayaran'])?></td>
                      <td><?= status_transaksi($value['status'])?></td>
                      <td>
                        <a href="" class="btn btn-primary btn-xs">Invoice</a>
                        <a href="" class="btn btn-danger btn-xs">Hapus</a>
                      </td>
                  </tr>                
              <?php endforeach ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>    
</div>