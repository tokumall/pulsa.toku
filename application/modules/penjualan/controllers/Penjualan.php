<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('penjualan_model');
		$penjualan = $this->penjualan_model->show(3);
		$data['penjualan'] = $penjualan;
		$data['page'] = array('penjualan','Berhasil','penjualan');
		$this->template->view('penjualan/vindex',$data);
	}
	public function waiting()
	{
		$this->load->model('penjualan_model');
		$penjualan = $this->penjualan_model->show(1);
		$data['penjualan'] = $penjualan;
		$data['page'] = array('penjualan','Menunggu','penjualan');
		$this->template->view('penjualan/vindex',$data);
	}
	public function process()
	{
		$this->load->model('penjualan_model');
		$penjualan = $this->penjualan_model->show(2);
		$data['penjualan'] = $penjualan;
		$data['page'] = array('penjualan','Sedang di proses','penjualan');
		$this->template->view('penjualan/vindex',$data);
	}
	public function refund()
	{
		$this->load->model('penjualan_model');
		$penjualan = $this->penjualan_model->show(5);
		$data['penjualan'] = $penjualan;
		$data['page'] = array('penjualan','Pengembalian','penjualan');
		$this->template->view('penjualan/vindex',$data);
	}
	public function cancel()
	{
		$this->load->model('penjualan_model');
		$penjualan = $this->penjualan_model->show(4);
		$data['penjualan'] = $penjualan;
		$data['page'] = array('penjualan','Pembatalan','penjualan');
		$this->template->view('penjualan/vindex',$data);
	}
	public function send()
	{
		$this->load->model('jabber');
		$this->jabber->send();
	}
}
