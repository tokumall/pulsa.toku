<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Template {
    protected $_ci;
     
    function __construct(){
        $this->_ci = &get_instance();
    }

	public function view($content, $value)
	{
		$data['page'] = $value['page'];
		$data['include_css'] = $this->_ci->load->view('template/include_css.php', $value, TRUE);
		$data['include_js'] = $this->_ci->load->view('template/include_js.php', $value, TRUE);
		$data['sidebar'] = $this->_ci->load->view('template/sidebar.php', $value, TRUE);
		$data['content'] = $this->_ci->load->view($content, $value, TRUE);

		$this->_ci->load->view('template/index.php', $data);
	}
}